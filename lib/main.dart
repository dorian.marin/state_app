import 'package:flutter/material.dart';
import 'dart:math';

void main() => runApp(const CoinFlip());

class CoinFlip extends StatefulWidget {
  const CoinFlip({super.key});

  @override
  State<CoinFlip> createState() => _CoinFlipState();
}

class _CoinFlipState extends State<CoinFlip> with WidgetsBindingObserver {
  // ------------------------- IMPORT

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print(state);
    super.didChangeAppLifecycleState(state);
  }

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(covariant CoinFlip oldWidget) {
    print('didUpdateWidget()');
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  int coinHead = 0;
  Random coinSide = Random();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Head or tail'),
        ),
        body: Center(
          child: Text(
            'The coin is: ${coinHead == 1 ? 'heads' : 'tails'}',
            style: const TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.attach_money),
          onPressed: () {
            setState(() {});
            coinHead = coinSide.nextInt(2);
            print(coinHead);
          },
        ),
      ),
    );
  }
}
